/**
 * Objetos
 * 
 */

// var persona = {
//     nombre: "Juan",
//     id: 1,
//     estado: true,
//     hijos: [],
//     direccion: {
//         calle: "calle falsa 123",
//         ciudad: ""
//     }
// };
// JSON

// persona.nombre;

// persona.estado = false;

/**
 * Funciones
 */
// var mensaje = function() {
//     console.log("este es el mensaje");
// };

// function mensajeV2(fun) {
//     fun();
//     console.log('Mensaje V2');
//     return 'Termino';
// }

// mensajeV2;
// mensaje();

(function() {

    var app = {
        version: '1.0.0',
        _charset: 'UTF-8',
        /**
         * funcion inicial
         */
        init: function() {

            console.log('STARTED');
            app.asignarEventos();

        },
        asignarEventos: function() {

            $('.js-mostrar').off('click').on('click', function() {
                app.fn.mostrarMensaje();
            });

            $('.js-agregar').off('click').on('click', function() {
                $('body').append('<button type="button" class="js-mostrar">mostrar-2</button>');
                app.asignarEventos();
            });
        },
        fn: {
            mostrarMensaje: function() {
                var nombre = $('#nombre').val();
                var apellido = $('#apellido').val();
                alert('HOLA ' + nombre + ' ' + apellido);
            }
        },
        /**
         * funciones publicas
         */
        publicFn: function() {
            var fnPublic = {
                asignarEventos: app.asignarEventos
            };
            return fnPublic;
        }
    };


    $(document).ready(function() {
        app.init();
    });
    /**
     * se hacen publicas las funciones en _cirugia
     */
    window._cirugia = app.publicFn();

    // window.app = app;

})();