

# Documentación

- ### HTML
    * [Introduction](https://docs.google.com/document/d/1jJUc2osJpSqIwPyc6U-QMpjFMEFyPgcsij14rWngVoA/edit?usp=sharing)

- ### CSS
    * [Mejorá tu CSS con OOCSS, BEM y SMACSS](http://apuntesalmargen.com/oocss.html)
    * [OOCSS](https://www.smashingmagazine.com/2011/12/an-introduction-to-object-oriented-css-oocss/)
